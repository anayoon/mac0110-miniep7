#MAC0110-MiniEP7
#Ana Yoon Faria de Lima - 11795273

function sin(x)
	i=0
	soma=0
	convert(BigInt,soma)
	while i<=10
		soma+=(-1)^(i)*big(x^(2i+1))/factorial(big(2i+1))
		i+=1
	end
	return soma
end

function cos(x)
	soma=0
	i = 0
	while i<=10
		soma+=(-1)^i*x^(2*i)/factorial(big(2*i))
		i +=1
	end
	return soma
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan(x)
	soma=0
	n=1
	contagem=1
	while contagem<=10
		soma=soma+((2^(2*n))*((2^(2*n))-1)*bernoulli(n)*x^((2*n)-1))/factorial(big(2*n))
		n=n+1
		contagem=contagem+1
	end
	return soma
end

function check_sin(value,x)
	erro=1e-3
	if (abs(sin(x)-value)<erro)
		return true
	else
		return false
	end
end

function check_cos(value,x)
	erro=1e-3
	if (abs(cos(x)-value)<erro)
		return true
	else
		return false
	end
end

function check_tan(value,x)
	erro=1e-2
	if (abs(tan(x)-value)<erro)
		return true
	else
		return false
	end
end

function taylor_sin(x)
	i=0
	soma=0
	convert(BigInt,soma)
	while i<=10
		soma+=(-1)^(i)*big(x^(2i+1))/factorial(big(2i+1))
		i+=1
	end
	return soma
end

function taylor_cos(x)
	soma=0
	i = 0
	while i<=10
		soma+=(-1)^i*x^(2*i)/factorial(big(2*i))
		i +=1
	end
	return soma
end

function taylor_tan(x)
	soma=0
	n=1
	contagem=1
	while contagem<=10
		soma=soma+((2^(2*n))*((2^(2*n))-1)*bernoulli(n)*x^((2*n)-1))/factorial(big(2*n))
		n=n+1
		contagem=contagem+1
	end
	return soma
end

function test()
	if   !check_sin(0.5,pi/6) || !check_sin(0,pi) || !check_sin(0,0)
		return "Erro. Verifique a funcao sin"
	end

	if   !check_cos(0.5,pi/3) || !check_cos(-1,pi) || !check_cos(0,pi/2)
		return "Erro. Verifique a funcao cos"
	end

	if   !check_tan(1,pi/4) || !check_tan(0,0) || !check_tan(1.732,pi/3)
		return "Erro. Verifique a funcao tan"
	end
	return "Tudo OK"
end

print(test())


